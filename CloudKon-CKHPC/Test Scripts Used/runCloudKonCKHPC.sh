#!/bin/sh

# E.g. sh runCloudKonCKHPC.sh numberOfJobsPerClient SleepLength numberOfTasksPerJob totalNumberOfWorkers portNumber numWorkerProcesses

if [ -z $1 ] || [ -z $2 ] || [ -z $2 ] || [ -z $3 ] || [ -z $4 ] || [ -z $5 ] || [ -z $6 ]
then
   echo "Please provide command line arguments."
   echo "Help Text: "
   echo "	sh runCloudKonCKHPC.sh numberOfJobsPerClient SleepLength numberOfTasksPerJob totalNumberOfWorkers portNumber numWorkerProcesses"
   exit
fi
#logs2000Jobs0SleepLen4Tasks16Nodes_4processes
numHosts=`cat newHostIp | grep -v "^$" | wc -l`
logDirectory="logs$1Jobs$2SleepLen$3Tasks$4Nodes$6Processes"
rm -rf $logDirectory
mkdir $logDirectory
prevResultDir=`awk '/DIRECTORY/ {print $NF}' analyzeCloudKonCKHPC.sh`
totalWorkerProcesses=$(($4 * $6))

# Prepare analyzeCloudKon file for this run.
sed "s/$prevResultDir/$logDirectory/g" analyzeCloudKonCKHPC.sh > temp
rm -rf analyzeCloudKonCKHPC.sh
cp temp analyzeCloudKonCKHPC.sh
rm temp

# Run client tasks
parallel-ssh -p $numHosts -t 0 -i -l ubuntu  -h newHostIp  -x " -o StrictHostKeyChecking=no -i CKHPC-key1.pem" "java -jar FC1.jar 1 $1 $2 $3 > `ifconfig | awk ' /inet addr/ {print $2}' | awk -F ':' '{if (NR == 1) {print $2}}'`_Client.out 2>&1 &";

portNumber=$5
for i in `seq 1 $6`
do
    portNumber=$(($portNumber + 1))
    # Run all the workers
   parallel-ssh -p $numHosts -t 0 -i -l ubuntu  -h newHostIp  -x " -o StrictHostKeyChecking=no -i CKHPC-key1.pem" "java -jar FW1.jar 1 1 false false $totalWorkerProcesses $portNumber > `ifconfig | awk ' /inet addr/ {print $2}' | awk -F ':' '{if (NR == 1) {print $2}}'`_Worker$logDirectory$portNumber.out 2>&1 &"
done

# Keep on checking whether worker instance is running or not if not running kick off the instance.
while [ 1 ]
do
    for ip in `cat newHostIp`
    do
#        retCode=`ps fx | grep -i "FalconWorkerRMIExceptionHandlingv1.6_WOSOP.jar" | grep -v "grep" | wc -l`
        retCode=`ssh  -i CKHPC-key1.pem ubuntu@$ip "ps fx | grep -i 'FW1.jar' | grep -v 'grep' | wc -l"`
        echo "Return code is: $retCode"
        if [ $retCode -eq 0 ]
        then
            echo "Kicking of the script on $ip"
            ssh -i CKHPC-key1.pem ubuntu@$ip "java -jar FW1.jar 1 1 false false $4 > FW1.jar_Worker.out 2>&1 &"
        fi
    done
done
