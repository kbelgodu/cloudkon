#!/bin/sh

parallel-scp -p 16 -t 0 -l ubuntu -h newHostIp -x " -o StrictHostKeyChecking=no -i FreeEC2Instance.pem" kill.sh /home/ubuntu/kill.sh
parallel-ssh -p 16 -t 0 -i -l ubuntu  -h newHostIp  -x " -o StrictHostKeyChecking=no -i FreeEC2Instance.pem" "sh kill.sh"
