#!/bin/sh

# DIRECTORY = logs10Jobs0SleepLen4Tasks16Nodes4Processes
i=1;
for ip in `cat newHostIp`
do 
    scp -i CKHPC-key1.pem ubuntu@$ip:~/output* ./logs10Jobs0SleepLen4Tasks16Nodes4Processes/output_$ip
    i=$(($i + 1))
done

for file in ./logs10Jobs0SleepLen4Tasks16Nodes4Processes/output_*
do 
    cat $file >> ./logs10Jobs0SleepLen4Tasks16Nodes4Processes/outputlogs10Jobs0SleepLen4Tasks16Nodes4Processes; 
done


#rm -rf ./logs10Jobs0SleepLen4Tasks16Nodes4Processes/output_*

i=1;
for ip in `cat newHostIp`
do

    for file in *_Workerlogs10Jobs0SleepLen4Tasks16Nodes4Processes*
    do
        scp -i CKHPC-key1.pem ubuntu@$ip:~/$file ./logs10Jobs0SleepLen4Tasks16Nodes4Processes/Workerlogs10Jobs0SleepLen4Tasks16Nodes4Processes_$i.out
#        scp -i CKHPC-key1.pem ubuntu@$ip:~/$file ./logs10Jobs0SleepLen4Tasks16Nodes4Processes/
        i=$(($i + 1))
    done
done
