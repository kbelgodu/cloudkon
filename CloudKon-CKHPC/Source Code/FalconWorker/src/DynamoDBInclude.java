import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
//import com.amazonaws.services.*;
//import com.amazonaws.services.dynamodbv2.*;
import com.amazonaws.services.dynamodbv2.model.*;

public class DynamoDBInclude {	
	static String tableName = "workerCheck_Test1";
	static AmazonDynamoDBClient HPCdynamoDB;
	Region usEast1;

	public DynamoDBInclude() throws AmazonServiceException, AmazonClientException{
		usEast1 = Region.getRegion(Regions.US_EAST_1);
		HPCdynamoDB = new AmazonDynamoDBClient(new ClasspathPropertiesFileCredentialsProvider());
		HPCdynamoDB.setRegion(usEast1);
	}	 

	public  void addHPCDynamoDB(Map<String, AttributeValue> newItem){
		PutItemRequest putItemRequest = new PutItemRequest(tableName, newItem);
		PutItemResult putItemResult = HPCdynamoDB.putItem(putItemRequest);
		//System.out.println("Item added" + putItemResult);
	}
	//	 
	private  Map<String, AttributeValue> addItem(long manager, long subworker) {
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put("worker", new AttributeValue("worker"));
		item.put("manager", new AttributeValue().withN(Long.toString(manager)));
		item.put("subworker", new AttributeValue().withN(Long.toString(subworker)));
		return item;
	}
	//	 
	public  void init(long manager, long subworker)
			throws AmazonServiceException, AmazonClientException{
		addHPCDynamoDB(addItem(manager, subworker));
	}
	//	 
	public void addToExisting(long noOfManagers, long noOfSubWorkers)
			throws AmazonServiceException, AmazonClientException{
		Map<String, AttributeValueUpdate> updateItems = 
				new HashMap<String, AttributeValueUpdate>();
		HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put("worker", new AttributeValue().withS("worker"));
		updateItems.put("manager",new AttributeValueUpdate().withAction(AttributeAction.ADD)
				.withValue(new AttributeValue().withN("+"+Long.toString(noOfManagers))));
		updateItems.put("subworker",new AttributeValueUpdate().withAction(AttributeAction.ADD)
				.withValue(new AttributeValue().withN("+"+Long.toString(noOfSubWorkers))));
		UpdateItemRequest updateItemRequest = new UpdateItemRequest()
		.withTableName(tableName).withKey(key).withAttributeUpdates(updateItems);	 

		HPCdynamoDB.updateItem(updateItemRequest);
	}

	public void deleteFromExisting(long noOfManagers, long noOfSubWorkers)
			throws AmazonServiceException, AmazonClientException{
		Map<String, AttributeValueUpdate> updateItems = 
				new HashMap<String, AttributeValueUpdate>();
		HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put("worker", new AttributeValue().withS("worker"));
		updateItems.put("manager",new AttributeValueUpdate().withAction(AttributeAction.ADD)
				.withValue(new AttributeValue().withN("-"+Long.toString(noOfManagers))));
		updateItems.put("subworker",new AttributeValueUpdate().withAction(AttributeAction.ADD)
				.withValue(new AttributeValue().withN("-"+Long.toString(noOfSubWorkers))));
		UpdateItemRequest updateItemRequest = new UpdateItemRequest()
		.withTableName(tableName).withKey(key).withAttributeUpdates(updateItems);	 

		HPCdynamoDB.updateItem(updateItemRequest);
	}	

	public long[] getData() throws AmazonServiceException, AmazonClientException {
		String s = "worker";
		long[] array = new long[2];
		int n = 0, i = 0;
		Map<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put("worker", new AttributeValue(s));
		GetItemRequest getItemRequest = new GetItemRequest()
		.withTableName(tableName)
		.withKey(key).withAttributesToGet(Arrays.asList("manager","subworker"));

		GetItemResult result = HPCdynamoDB.getItem(getItemRequest);
		Map<String, AttributeValue> attributeList = result.getItem();
		for (Map.Entry<String, AttributeValue> item : attributeList.entrySet()) {
			AttributeValue value = item.getValue();
			n = Integer.parseInt((value.getN() == null ? "" :value.getN()));
			array[i++] = n;
		}
		return array;
	}


	public boolean ForWorkerCreationCheck(long numOfWorkers, long totolNumOfWorkers) 
			throws AmazonServiceException, AmazonClientException{
		long[] dataArray = this.getData();
		long master = dataArray[0];
		long worker = dataArray[1];
		double req = worker+numOfWorkers-master-1;
		double avail = totolNumOfWorkers-master-1;
		//System.out.println("Available Workers: " + avail + "\n Required Workers: " + req + "\n Cond value: " + (1.25*req));
		if( (avail > 1.25*req) && (master <= worker) && (worker < totolNumOfWorkers) ){
			// possible values can be .25 

			this.addToExisting( 1, numOfWorkers);
			//System.out.println("Returning true you may execute your task");
			long[] dataArray2 = this.getData();
			long newMaster = dataArray2[0];
			long newWorker = dataArray2[1];
			double newReqWorkers = newWorker - newMaster;
			double newAvailWorkers = totolNumOfWorkers - newMaster;
			//System.out.println("New updated data:");
			//System.out.println("Available Workers: " + newAvailWorkers + "\n Required Workers: " + newReqWorkers + "\n Cond value: " + (1.25*newReqWorkers));
			if((newAvailWorkers > 1.25*newReqWorkers) && (newMaster <= newWorker) && (newWorker < totolNumOfWorkers)) {
				return true;
			} else {
				//				System.out.println("ALERT\t: Simultaneous update. Releasing this task.");
				this.deleteFromExisting(1, numOfWorkers);
				return false;
			}
		}
		else {
			//System.out.println("Worker will not be able to run the task.");
			return false;
		}
	}












	/*
	 * Function		: hasDeadlockOccur
	 * Description	: To check whether is there any deadlock or not.
	 */
	public boolean checkDeadlock(long totolNumOfWorkers) throws AmazonServiceException, AmazonClientException{
		
		boolean hasDeadlockOccured = true;
		int tryCount = 0;

		while (tryCount < 5) {
			long[] dataArray = this.getData();
			long master = dataArray[0];
			long worker = dataArray[1];
			double req = worker-master;
			double avail = totolNumOfWorkers-master;
			
			if( (avail > 1.25*req) && (master <= worker) && (worker < totolNumOfWorkers) ){
				hasDeadlockOccured = false;
				break;
			}
			
			try {
				Thread.sleep(2);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
			
			tryCount++;
		}
		return hasDeadlockOccured;
	}
	/*public static void main(String[] args){
		 DynamoDBInclude myDynamo = new DynamoDBInclude();
		 myDynamo.init( 2 , 6);
		 long[] array = myDynamo.getData();
		 for (long a : array){
			 System.out.println(a);
		 }

		 System.out.println(myDynamo.ForWorkerCreationCheck(3, 16));

//		 myDynamo.addToExisting(1,10);
//		 System.out.println("adding 1 and 10");
//		 array = myDynamo.getData();
//		 for (long a : array){
//			 System.out.println(a);
//		 }
//		 myDynamo.addToExisting(1,5);
//		 System.out.println("adding 1 and 5");
//		 array = myDynamo.getData();
//		 for (long a : array){
//			 System.out.println(a);
//		 }
//		 myDynamo.addToExisting(1,15);
//		 System.out.println("adding 1 and 15 again");
//		 array = myDynamo.getData();
//		 for (long a : array){
//			 System.out.println(a);
//		 }
//		 
//		 
//		 myDynamo.deleteFromExisting(1,10);
//		 System.out.println("deleting 1 and 10");
//		 array = myDynamo.getData();
//		 for (long a : array){
//			 System.out.println(a);
//		 }
//		 myDynamo.deleteFromExisting(1,5);
//		 System.out.println("deleting 1 and 5");
//		 array = myDynamo.getData();
//		 for (long a : array){
//			 System.out.println(a);
//		 }
//		 myDynamo.deleteFromExisting(1,15);
//		 System.out.println("deleting 1 and 15 again");
//		 array = myDynamo.getData();
//		 for (long a : array){
//			 System.out.println(a);
//		 }
		 }*/
}

