import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class RemoteWorker {

    public static void main(String[] args) {
    	int poolSize = Integer.valueOf(args[0]);
    	int processMaxCount = Integer.valueOf(args[1]);
    	boolean duplicateCheck = Boolean.valueOf(args[2]); 
    	boolean monitoring = Boolean.valueOf(args[3]);
    	long totNumOfWorkers = Long.valueOf(args[4]);
    	//TODO change default port
    	int defaultPort = Integer.valueOf(args[5]);
    	//int defaultPort = 8007;
    	int workerThreadPort;
    	ExecutorService  pool = Executors.newFixedThreadPool(poolSize);
    	for (int i = 0; i < poolSize; i++) {
    		workerThreadPort = defaultPort;// + i;
    	    pool.submit(new WorkerThread(processMaxCount,duplicateCheck,monitoring, workerThreadPort, totNumOfWorkers));
    	}
    	
    	pool.shutdown();
    	
    	
    	/*public static void main(String[] args) throws RemoteException, MalformedURLException, NotBoundException, InterruptedException, ExecutionException {
    		runWorker wm = new runWorker();
    		int wmPort = 6066;
    		if(args[0].equalsIgnoreCase("server")) {
    			wm.runServer(1000, getWorkerIpAddress(), wmPort, 1, 2);
    		} else if (args[0].equalsIgnoreCase("client")) {
    			int port = Integer.valueOf(args[1]);
    			int retCode = wm.runClient(getWorkerIpAddress(), 6066, getWorkerIpAddress(), port);
    			System.out.println("Return code is: " + retCode);
    		}*/
    		
    	}
}
