import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.security.crypto.codec.Base64;

import sqsMessages.format.HpcWorkerMessage.hpcWorkerMessage;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class runWorker implements hpcClientServer {

	static long currentNumWorkers;
	static long requiredNumWorkers;
	static int numOfResponse;
	String workerManagerIP;
	int port;
	Registry registry;
	static public List<String> registryBindNames;// = new ArrayList<String>();
	static public List<String> registryBindNamesStayConnected;
	static long sleepLength = -1;
	static boolean ownTask; 
	static boolean gotTask = false; 
	static boolean leaveTask = false;
	static long currentTaskId;
	static boolean responseCheck;
	boolean sendNeedResponse = false;
	boolean taskNotDispatched = true;
	boolean waitForTaskFromManager = true;
	static long subworkerTimeoutThreshold = 60000;

	protected runWorker() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}


	/*
	 * Function		: notifyManager(non-Javadoc)
	 * Description	: Notify manager for sharing workload and share sub-workers registry information.
	 */
	//public boolean notifyManager(String hpcClientRegistry, long clientHpcTaskId) throws RemoteException {
	public long notifyManager(String hpcClientRegistry, long clientHpcTaskId) throws RemoteException {
		if ((clientHpcTaskId == this.currentTaskId) && (this.currentNumWorkers < this.requiredNumWorkers)) {
			//		if(currentNumWorkers < requiredNumWorkers) {
			//System.out.println(this.port +": Info\t: Client " + hpcClientRegistry + " requested to share HPC workload.");
			synchronized(this) {
				this.currentNumWorkers ++;
			}
			if(this.currentNumWorkers <= this.requiredNumWorkers) {
				System.out.println("Info\t: Excellant worker '" + hpcClientRegistry + "' will work with me on HPC task.");
				synchronized(this) {
					registryBindNames.add(hpcClientRegistry);
				}
				//return true;
				return this.subworkerTimeoutThreshold;
			} else {
				System.out.println("Info\t: Rejected worker '" + hpcClientRegistry + "' for HPC task.");
				//return false;
				return -1;
			}
		} else {
			//System.out.println(this.port +": Info\t: Currently, working on task with id: " + this.currentTaskId);
			//System.out.println(this.port +": Info\t: Worker '" + hpcClientRegistry +"' contacted for task " + clientHpcTaskId);
			//System.out.println(this.port +": Info\t: This client will not be used.");
			//return false;
			return -1;
		}
	}


	/*
	 * Function		: notifyClientOfTask
	 * Description	: Notify client of task and send them a task.
	 */
	public boolean notifyClientOfTask(long sleepTime) throws RemoteException {
		System.out.println("Info\t: Got notification from worker manager for work");
		this.gotTask = true;
		this.sleepLength = sleepTime;
		//System.out.println(this.port +": Info\t: Sleep time is: " + sleepLength);
		// boolean to check if the client successfully received the notifyClientOfTask

		return true;
	}


	/* 
	 * Function		: notifyResponse
	 * Description	: Function to notify server that client has finished its work.
	 */
	public int notifyResponse(String bindName) throws RemoteException {

		if (sendNeedResponse == true && (registryBindNamesStayConnected.contains(bindName))){ 
			registryBindNamesStayConnected.remove(bindName);
			return -1;
		}else {		// send successful response to manager	
			//System.out.println("Info\t: Client worker has sent his response");
			synchronized(this) {
				numOfResponse++;
			}
			System.out.println("Info\t: Till now received " + numOfResponse + "responses.");
			return 0;
		}
	}

	/*
	 * Function		: notifyLeaveTask
	 * Description	: This function will notify client to not to wait for the task
	 */
	public boolean notifyLeaveTask() throws RemoteException {
		System.out.println(this.port +": Info\t: Worker manager has notified to leave this task.");
		this.leaveTask = true;
		//System.out.println(this.port +": Info\t: Leaving this task! Bye bye!");
		return true;
	}

	/*
	 * Function		: isThisTaskAlive
	 * Description	: Check whether the client or server with this task is alive or not.
	 */
	public boolean isThisTaskAlive(long taskId) throws RemoteException {
		if (this.currentTaskId == taskId) {
			return true;
		} else {
			return false;
		}
	}


	/*
	 * Function		: runTask
	 * Description	: Execute the task.
	 */
	public void runTask(long sleepTime) {
		System.out.println(this.port +": Info\t: Sleeping for: " + sleepTime);
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(this.port +": Info\t: I am awake now");
	}



	/*
	 * Function		: runServer
	 * Description	: This will run the worker node as a server,
	 * 				  who will distribute HPC tasks to other sub-worker.
	 * 				  Work flow:
	 * 				1. Wait until worker gets all required workers.
	 * 				2. Once got all the workers, notify them of the task.
	 * 				3. Execute its, own task.
	 * 				4. Wait for all workers to finish their work and get response.  
	 */
	public int runServer(long taskId, final int sleepLength, String workerIP, int port, long cnw, long rnw, boolean createRegistry, long maxWorkers) throws MalformedURLException, RemoteException, NotBoundException, InterruptedException, ExecutionException {

		this.workerManagerIP = workerIP;
		this.port = port;
		this.currentNumWorkers = cnw;
		this.requiredNumWorkers = rnw;
		this.numOfResponse = 1;
		this.registryBindNames = new ArrayList<String>();
		this.registryBindNamesStayConnected = new ArrayList<String>();
		List<String> registryBindNamesTaskGiven  = new ArrayList<String>();
		this.ownTask = true;
		this.currentTaskId = taskId;
		DynamoDBInclude hpcDynamo  = new DynamoDBInclude();
		int runCompleteRetCode = 1;

		//System.out.println(this.port +": SERVER \t:: Binding name");
		String bindName = this.workerManagerIP.concat(":").concat(String.valueOf(this.port));
		hpcClientServer wm = new runWorker();
		try {
			//System.out.println(this.port +": SERVER \t:: exporting object (stub)");
			hpcClientServer stub = (hpcClientServer) UnicastRemoteObject.exportObject(wm, this.port);
			try {
				if(createRegistry) {
					registry = LocateRegistry.createRegistry(this.port);
				} else {
					registry = LocateRegistry.getRegistry(this.port);
				}
				registry.rebind(bindName, stub); 

				// Put all the messages in HPC worker queue.
				putMessages((rnw-1));
				
				System.out.println("SERVER \t:: Waiting for all the workers");
				runCompleteRetCode = 1;
				//int ckeckDeadlockCnt = 0;

				//while(taskNotDispatched && (ckeckDeadlockCnt < 3)) {
				while(taskNotDispatched) {

					// ******* Make all the timers dynamic *******
					int threshold = (sleepLength + 20000);
					long timeoutThreshold = requiredNumWorkers*threshold;
					long timing = System.currentTimeMillis();
					long waitTime = 0;  

					while(((registryBindNames.size()+1) != requiredNumWorkers) && (timeoutThreshold > waitTime)) {
						// Wait until either timer expires or worker gets required number of workers.
						waitTime = System.currentTimeMillis()-timing;
						this.subworkerTimeoutThreshold = (timeoutThreshold - waitTime + 5000);
					}

					if ((registryBindNames.size()+1) != requiredNumWorkers) {
						/* 
						 * If we reach here, that means timer has expired. 
						 * If it is expired, check the DynamoDB status.
						 */

						/*
						 * If DynamoDB status shows there is deadlock,
						 * 1. Release the task.
						 * 2. Notify all the workers to leave this task.
						 * If there is no deadlock
						 * 1. Increase the deadlock check counter.
						 * 2. And prepare to wait for more time.
						 */

						System.out.println("WARNING\t: Ohh! Timer Expired");
						//ckeckDeadlockCnt++;
						boolean hasDeadlockOccurred = false;

						try {
							hasDeadlockOccurred = hpcDynamo.checkDeadlock(maxWorkers);
						} catch (AmazonServiceException ase) {
							// Caught an exception while working with dynamoDB.
							// No need to run the task and put back the message into the queue.
							hasDeadlockOccurred = false;
							System.out.println("ERROR\t: Faced issues while running DynamoDB");
							System.out.println(ase.getMessage());
							ase.printStackTrace();
						} catch (AmazonClientException ace) {
							// Caught an exception while working with dynamoDB.
							// No need to run the task and put back the message into the queue.
							hasDeadlockOccurred = false;
							System.out.println("ERROR\t: Faced issues while running DynamoDB");
							System.out.println(ace.getMessage());
							ace.printStackTrace();
						}
						//}

						if(hasDeadlockOccurred) {
							UnicastRemoteObject.unexportObject(wm, true);
							for (String registryName : registryBindNames) {
								/* Using registry binding name of each client associated with this worker
								 * notify each of them to leave this task.
								 */
								String[] clientWorkerDetails = registryName.split(":");
								Registry registry = LocateRegistry.getRegistry(clientWorkerDetails[0], Integer.valueOf(clientWorkerDetails[1]));
								hpcClientServer comp = (hpcClientServer) registry.lookup(registryName);

								int tryCount = 0;				// If get an exception, try for 3 times else leave.
								boolean isNotified = false;
								boolean caughtException = true;		// Flag to check whether we caught an exception or not.

								while(caughtException && tryCount < 3) {
									/*
									 * Notify manager to share HPC workload.
									 * If we get an exception while accessing remote object, retry for two more times.
									 * If notified successfully, proceed accordingly.
									 */
									try {
										isNotified = comp.notifyLeaveTask();
										caughtException = false;
									} catch (ConnectException ce) {
										System.out.println("ERROR\t: Faced issues while notifying - " + ce.getMessage());
										tryCount++;
										runCompleteRetCode = 1;
										waitRandom();
										//ce.printStackTrace();
									} catch (RemoteException re) {
										System.out.println("ERROR\t: Faced issues while notifying - " + re.getMessage());
										tryCount++;
										runCompleteRetCode = 1;
										waitRandom();
										//re.printStackTrace();
									}
								}
							}
							runCompleteRetCode = 1;
							taskNotDispatched = false;
						}

					} else {
						/* If we got all workers then proceed
						 * Notify each of the sub workers (client workers) to execute the tasks.
						 * Execute its own task. And get all the responses back.
						 */
						System.out.println(this.port +": Info\t: Got all the workers");
						//if (currentNumWorkers == requiredNumWorkers) {
						try {

							// Call notifyClientOfTask using remote object
							//System.out.println(this.port +": SERVER \t:: Notifying all the client workers");
							for (String registryName : registryBindNames) {
								// Notify each client of the task.
								boolean gaveTask = false;
								String[] clientWorkerDetails = registryName.split(":");
								Registry registry = LocateRegistry.getRegistry(clientWorkerDetails[0], Integer.valueOf(clientWorkerDetails[1]));
								hpcClientServer comp = (hpcClientServer) registry.lookup(registryName);

								int tryCount = 0;				// If get an exception, try for 3 times else leave.
								boolean isNotified = false;
								boolean caughtException = true;		// Flag to check whether we caught an exception or not.

								while(caughtException && tryCount < 3) {
									/*
									 * Notify manager to share HPC workload.
									 * If we get an exception while accessing remote object, retry for two more times.
									 * If notified successfully, proceed accordingly.
									 */
									try {
										isNotified = comp.notifyClientOfTask(sleepLength);
										caughtException = false;
										this.taskNotDispatched = false;
									} catch (ConnectException ce) {
										System.out.println("ERROR\t: Faced issues while notifying - " + ce.getMessage());
										tryCount++;
										runCompleteRetCode = 1;
										waitRandom();
										//ce.printStackTrace();
									} catch (RemoteException re) {
										System.out.println("ERROR\t: Faced issues while notifying - " + re.getMessage());
										tryCount++;
										runCompleteRetCode = 1;
										waitRandom();
										//re.printStackTrace();
									}
								}

								//gaveTask = comp.notifyClientOfTask(sleepLength);
								gaveTask = isNotified;
								if(!gaveTask) {
									// If client failed to notify sub-worker of running task.
									// Try two more times.
									//System.out.println(this.port +": SERVER \t:: Failed to give task [" + registryName + "]. Trying again.");
									for(int tryGiveTask = 1; tryGiveTask < 3; tryGiveTask++) {
										gaveTask = comp.notifyClientOfTask(sleepLength);
										if(gaveTask) {
											// Task to the client sent successfully.
											//System.out.println(this.port +": SERVER \t:: Task sent successfully.");
											break;
										}
									}
									if(!gaveTask) {
										// Worker manager could not get to the sub worker to give him a task.
										// So let other workers wait.
										// Put a message in the request queue for one more worker.
										// And for getting one more worker.
										registryBindNames.remove(registryName);
										this.currentNumWorkers--;
										this.sendNeedResponse = true;
										this.taskNotDispatched = true;
										registryBindNamesStayConnected = registryBindNamesTaskGiven;
										putMessages(1);
										break;
									} else {
										this.taskNotDispatched = false;
										// Sub worker got task. Add it to the list.
										registryBindNamesTaskGiven.add(registryName);
									}
								} else {
									// Sub worker got task. Add it to the list of workers who got the task.
									this.taskNotDispatched = false;
									registryBindNamesTaskGiven.add(registryName);
								}

							}

							if(!this.taskNotDispatched) {
								// Run the task, if manager was successful to notify each subworker of the task.
								Thread workerManagerTask = new Thread() {
									public void run() {
										//System.out.println(": SERVER \t:: Thread started");
										runTask(sleepLength);
										//System.out.println(": SERVER \t:: Notifying main thread to proceed");
										ownTask = false;
									}
								};
								workerManagerTask.start();

								System.out.println("SERVER \t:: Waiting for response");

								threshold = (sleepLength + 60000);
								timeoutThreshold = requiredNumWorkers*threshold;
								timing = System.currentTimeMillis();
								waitTime = 0;  

								while (((numOfResponse != requiredNumWorkers) || (ownTask == true)) && (timeoutThreshold > waitTime)) {
									// Do nothing. Wait until manager gets response from all the workers. 
									waitTime = System.currentTimeMillis()-timing;
									//System.out.println(this.port +": Own Task : "+ ownTask);
									//System.out.println("All responses? " + (numOfResponse != requiredNumWorkers));
								}
								if ((timeoutThreshold <= waitTime)) {
									System.out.println("Timer Expired. Proceeding further.");
									runCompleteRetCode = 1;
								} else {
									System.out.println("SERVER \t:: Got all the responses");
									runCompleteRetCode = 0;
								}

							} else {
								/* This means worker manager could not notify one of the 
								 * client worker of task, so he will again work on the same
								 *  task and will wait for all the workers.
								 */
								System.out.println(this.port +": SERVER \t:: Failed to send task to one of the worker.");
								System.out.println(this.port +": SERVER \t:: Waiting for few more workers.");
							}


						} catch(NotBoundException nbe) {
							System.out.println("ERROR\t: " + nbe.getMessage());
							nbe.printStackTrace();
							this.taskNotDispatched = false;
							runCompleteRetCode = 1;
						} catch(RemoteException re) {
							System.out.println("ERROR\t: " + re.getMessage());
							re.printStackTrace();
							this.taskNotDispatched = false;
							runCompleteRetCode = 1;
						}
					}
				}
				// Once everything goes fine unbind and unexport the remote object.
				registry.unbind(bindName);
				UnicastRemoteObject.unexportObject(wm, true);
				return runCompleteRetCode;

			} catch (RemoteException re) {
				System.out.println("ERROR\t: " + re.getMessage());
				UnicastRemoteObject.unexportObject(wm, true);
				re.printStackTrace();
				return 1;
			}

		} catch (RemoteException re) {
			System.out.println("ERROR\t: " + re.getMessage());
			re.printStackTrace();
			return 1;
		}
	}

	private void putMessages(long numMessages) {
		// TODO Auto-generated method stub
		AmazonSQS sqs = new AmazonSQSClient(new ClasspathPropertiesFileCredentialsProvider());
		byte[] encoded;
		hpcWorkerMessage.Builder hpcQueueMsg = hpcWorkerMessage.newBuilder();
		GetQueueUrlRequest getHpcWorkerQueueUrlRequest = new GetQueueUrlRequest("hpcWorkerManager1");        // Pank-Hpc
		String requestHpcWorkerMessageQueueUrl = sqs.getQueueUrl(getHpcWorkerQueueUrlRequest).getQueueUrl();

		hpcQueueMsg.setTaskId(this.currentTaskId);
		hpcQueueMsg.setWorkerIp(this.workerManagerIP);
		hpcQueueMsg.setWorkerSocketPort(this.port);
		hpcQueueMsg.setPickUpCount(0);
		//hpcQueueMsg.setTaskPriority(task.getTaskPriority);
		encoded = hpcQueueMsg.build().toByteArray();
		String stringTask = new String(Base64.encode(encoded));
		
		for(int num = 0; num < numMessages; num++) {
			//System.out.println("Info\t: Message " + num + ": Putting message into the HPC woker manager queue");
			sqs.sendMessage(new SendMessageRequest(requestHpcWorkerMessageQueueUrl, stringTask));
		}
	}


	public void waitRandom() {
		Random rand = new Random();
		int randomNumber = rand.nextInt(5);
		try {
			Thread.sleep(randomNumber);
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
	}
	/*
	 * Function		: runClient
	 * Description	: This function will run the worker node in client mode, 
	 * 				  where it will help worker manager to carry our HPC tasks.
	 * 				  Work Flow:
	 * 				1. Notify manager to share workload.
	 * 				2. Get task from worker manager and execute it.
	 * 				3. Send response back to the manager.
	 */
	public int runClient(long hpcTaskId, String workerMgrIP, int workerMgrPort, String ownIP, int ownPort, boolean createRegistry) throws MalformedURLException, RemoteException, NotBoundException, InterruptedException {

		this.gotTask = false;
		this.leaveTask = false;
		// Following variables are declared so that if the worker is running as a client and any other worker comes and talks with him for connection due to some previous stale/duplicate message in the HPC queue, then this worker should be able to reject the connection. so that other worker can go ahead and ask for the connection. 
		this.currentNumWorkers = 2;
		this.requiredNumWorkers = 1;
		this.currentTaskId = hpcTaskId;
		int returnCode = 1;

		//System.out.println(ownPort +": CLIENT \t:: Creating bind string");
		String bindName = ownIP.concat(":").concat(String.valueOf(ownPort));
		hpcClientServer wm = new runWorker();

		try {
			//System.out.println(ownPort +": CLIENT \t:: Exporting object");
			hpcClientServer stub = (hpcClientServer) UnicastRemoteObject.exportObject(wm, ownPort);

			try {
				if(createRegistry) {
					registry = LocateRegistry.createRegistry(ownPort);
				} else {
					registry = LocateRegistry.getRegistry(ownPort);
				}
				registry.rebind(bindName, stub); 

				//System.out.println(ownPort +": CLIENT \t:: Bind string for manager");
				String managerRegistryName = workerMgrIP.concat(":").concat(String.valueOf(workerMgrPort));
				//Registry registry = LocateRegistry.getRegistry(workerManagerIP);

				try {
					Registry registry = LocateRegistry.getRegistry(workerMgrIP, workerMgrPort);
					hpcClientServer comp = (hpcClientServer) registry.lookup(managerRegistryName);

					//System.out.println("CLIENT \t:: Notifying manager");
					//long timeoutThreshold = comp.notifyManager(bindName, this.currentTaskId);

					//if(timeoutThreshold != -1) {
					int tryCount = 0;				// If get an exception, try for 3 times else leave.
					boolean notifiedManager = false;	// Flag to find whether manager was notified or not.
					boolean caughtException = true;		// Flag to check whether we caught an exception or not.
					long timeoutThreshold = -1;
					
					while(caughtException && tryCount < 3) {
						/*
						 * Notify manager to share HPC workload.
						 * If we get an exception while accessing remote object, retry for two more times.
						 * If notified successfully, proceed accordingly.
						 */
						try {
							timeoutThreshold = comp.notifyManager(bindName, this.currentTaskId);
							caughtException = false;
						} catch (ConnectException ce) {
							System.out.println("ERROR\t: Faced issues while notifying - " + ce.getMessage());
							tryCount++;
							returnCode = 1;
							waitRandom();
							//ce.printStackTrace();
						} catch (RemoteException re) {
							System.out.println("ERROR\t: Faced issues while notifying - " + re.getMessage());
							tryCount++;
							returnCode = 1;
							waitRandom();
							//re.printStackTrace();
						}
					}

					if((timeoutThreshold != -1)) {
						notifiedManager = true;
					}
					
					if(notifiedManager) {
						/* 
						 * Worker manager accepted subworkers request to work with him.
						 * Wait for worker manager to
						 * 1. Notify sub-worker of task.
						 * 2. Notify sub-worker to leave this task (valid in case of deadlock or
						 * manager failed to notify sub-worker to leave this task)
						 */
						System.out.println("CLIENT \t:: Waiting for task");
						while(waitForTaskFromManager){

							long timing = System.currentTimeMillis();
							long waitTime = 0;
							while (!this.gotTask && !this.leaveTask && (timeoutThreshold > waitTime)) {
								//while (!this.gotTask && !this.leaveTask) {
								/*
								 * Wait for the task until, either of the following occurs:
								 * 1. Manager notifies worker of the task. (Manager got all the workers.)
								 * 2. Manager notifies worker to leave this task. (Manager timer expires.)
								 * 3. Subworker timer expires. (Manager failed to follow the deadline :P)
								 */
								waitTime = System.currentTimeMillis()-timing;
							}

							if (this.gotTask) {
								System.out.println("CLIENT \t:: Running task");
								runTask(sleepLength);
								System.out.println("CLIENT \t:: Sending response");

								// when Manager asks him to stay for another run of the same task value = -1
								// when client is unable to send response back to manager then value = 1
								// when client is successfully able to send response back to manager then value = 0

								int sentResponseToManager = 1;
								tryCount = 0;				// If get an exception, try for 3 times else leave.
								caughtException = true;		// Flag to check whether we caught an exception or not.

								while(caughtException && tryCount < 3) {
									/*
									 * If sub-worker face any issues while sending response to manager,
									 * try for two more times.
									 */
									try {
										sentResponseToManager = comp.notifyResponse(bindName);
										caughtException = false;
									} catch (ConnectException ce) {
										System.out.println("ERROR\t: Faced issues while sending response - " + ce.getMessage());
										tryCount++;
										returnCode = 1;
										waitRandom();
										//ce.printStackTrace();
									} catch (RemoteException re) {
										System.out.println("ERROR\t: Faced issues while sending response - " + re.getMessage());
										tryCount++;
										returnCode = 1;
										waitRandom();
										//re.printStackTrace();
									}
								}

								/*if (sentResponseToManager == 1) // retry sending the response
								{
									for (int i=1; i<=4; i++){
										sentResponseToManager = comp.notifyResponse(bindName);
										if (sentResponseToManager != 1){
											waitForTaskFromManager = false;
											break;
										}
									}
								}*/

								if (sentResponseToManager == -1){ // manager has asked to wait for another run of the same task
									// wait for task for re-run from manager
									this.gotTask = false;
									waitForTaskFromManager = true;
								} else if (sentResponseToManager == 0) {
									// Successfully sent the task.
									waitForTaskFromManager = false;
									returnCode = 0;
								} else {
									// If not able send response to worker manager. Leave it.
									waitForTaskFromManager = false;
									returnCode = 0;
								}
							} else if(this.leaveTask) { 
								System.out.println(ownPort +": CLIENT WARNING\t: Manager requested to leave this task");
								waitForTaskFromManager = false;
								returnCode = 0;
							} else {
								System.out.println("CLIENT WARNING\t: Manager failed to send notification withing the specified time.");

								tryCount = 0;				// If get an exception, try for 3 times else leave.
								boolean isTaskAlive = false;	// Flag to find whether task is alive or not.
								caughtException = true;		// Flag to check whether we caught an exception or not.

								while(caughtException && tryCount < 3) {
									/*
									 * Ask manager whether this task is still alive or not.
									 * If task is valid, wait for the manager's notification for the specified time.
									 * Else, leave this task.
									 * If got any exception while checking, whether the task is alive or not.
									 * Try 2 more times. If still no success, leave the task.
									 */
									try {
										isTaskAlive = comp.isThisTaskAlive(this.currentTaskId);
										caughtException = false;
									} catch (ConnectException ce) {
										System.out.println("ERROR\t: Faced issues while checking task aliveness - " + ce.getMessage());
										tryCount++;
										returnCode = 0;
										waitRandom();
										//ce.printStackTrace();
									} catch (RemoteException re) {
										System.out.println("ERROR\t: Faced issues while checking task aliveness - " + re.getMessage());
										tryCount++;
										returnCode = 0;
										waitRandom();
										//re.printStackTrace();
									}
								}

								if(!isTaskAlive) {
									waitForTaskFromManager = false;
									returnCode = 0;
								}
							}
						}
					}
					//registry.unbind(bindName);
					UnicastRemoteObject.unexportObject(wm, true);
					return returnCode;

				} catch (ConnectException ce) {
					System.out.println("ERROR\t: " + ce.getMessage());
					UnicastRemoteObject.unexportObject(wm, true);
					ce.printStackTrace();
					return 1;
				} catch (NotBoundException nbe) {
					System.out.println("ERROR\t: " + nbe.getMessage());
					UnicastRemoteObject.unexportObject(wm, true);
					nbe.printStackTrace();
					return 1;
				} catch (RemoteException re) {
					System.out.println("ERROR\t: " + re.getMessage());
					UnicastRemoteObject.unexportObject(wm, true);
					re.printStackTrace();
					return 1;
				}
			} catch (RemoteException re) {
				System.out.println("ERROR\t: " + re.getMessage());
				UnicastRemoteObject.unexportObject(wm, true);
				re.printStackTrace();
				return 1;
			}
		} catch (RemoteException re) {
			System.out.println("ERROR\t: " + re.getMessage());
			re.printStackTrace();
			return 1;
		}
	}
}