import java.rmi.*;

public interface hpcClientServer extends Remote {
	/* This interface will help HPC client and HPC server to run the HPC task in following ways:
	 * 1. Notify server to work with him
	 * 2. Run the task on server and client both
	 * 3. Notify server after finishing the task.
	 */
	
	public long notifyManager(String hpcClientRegistry, long currentTaskId) throws RemoteException;
	public boolean notifyClientOfTask(long sleepTime) throws RemoteException;
	public int notifyResponse(String hpcClientRegistry) throws RemoteException;
	public boolean notifyLeaveTask() throws RemoteException; 
	public boolean isThisTaskAlive(long taskId) throws RemoteException;
}
																										