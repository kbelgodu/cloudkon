import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.io.*;
import java.net.*;

import org.springframework.security.crypto.codec.Base64;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeAction;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
//import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
//import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
//import com.amazonaws.services.dynamodbv2.model.PutItemResult;
//import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.google.protobuf.InvalidProtocolBufferException;












//import com.iman.scheduler.message.TaskMessage.Task;
import sqsMessages.format.TaskMessage.Task;
import sqsMessages.format.HpcWorkerMessage.hpcWorkerMessage;
import sqsMessages.format.HpcWorkerMessage.hpcWorkerMessage.Builder;

public class WorkerThread implements Runnable{

	int duplicate = 0;
	boolean isBusy = false;
	boolean isDone = false;
	boolean duplicateCheck = false;
	boolean monitoring = false;
	AmazonSQS sqs;
	String QueueUrlPrefix=null;
	DynamoDBInclude hpcDynamo;
	int numberofMaster;
	int numberofWorkers;
	static String tableName = "messages";
	static AmazonDynamoDBClient dynamoDB;
	Collection<String> attributeNames;
	static String workerId;
	public int processMaxCount;
	public int workerThreadPort;
	String requestQueueUrl;
	String requestHpcWorkerMessageQueueUrl;         // Pank-Hpc
	public boolean createRegistry = true;
	public long totNumOfWorkers;
	/*long prevTaskTimeTaken = 0;
	long prevTaskNumWorkers = 1;
	long timeoutThreshold = 120000;*/

	public WorkerThread(int processMaxCount,boolean duplicateCheck,boolean monitoring, int workerThreadPort, long totNumOfWorkers) {

		sqs = new AmazonSQSClient(new ClasspathPropertiesFileCredentialsProvider());
		Region usEast1 = Region.getRegion(Regions.US_EAST_1);
		sqs.setRegion(usEast1);
		dynamoDB = new AmazonDynamoDBClient(new ClasspathPropertiesFileCredentialsProvider());
		dynamoDB.setRegion(usEast1);
		hpcDynamo = new DynamoDBInclude(); //TODO
		this.attributeNames = new ArrayList<String>();
		this.attributeNames.add("ApproximateFirstReceiveTimestamp");
		this.attributeNames.add("SentTimestamp");
		workerId =  UUID.randomUUID().toString();
		this.processMaxCount = processMaxCount;
		this.duplicateCheck = duplicateCheck;
		this.monitoring = monitoring;
		this.workerThreadPort = workerThreadPort;
		GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest("ThroughputMeasure1");
		requestQueueUrl = sqs.getQueueUrl(getQueueUrlRequest).getQueueUrl();        
		GetQueueUrlRequest getHpcWorkerQueueUrlRequest = new GetQueueUrlRequest("hpcWorkerManager1");        // Pank-Hpc
		requestHpcWorkerMessageQueueUrl = sqs.getQueueUrl(getHpcWorkerQueueUrlRequest).getQueueUrl();        // Pank-Hpc
		QueueUrlPrefix=requestQueueUrl.substring(0,requestQueueUrl.lastIndexOf('/')+1);
		this.totNumOfWorkers = totNumOfWorkers;
	}

	/*
	 * Function	: threadSleep
	 * Details	: Run system sleep function to execute task.
	 */
	private void threadSleep(long sleepLength) {
		try {
			//System.out.println(isBusy);
			//System.out.println("going to sleep for "+ sleepLength);
			Thread.sleep(sleepLength);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Function	: sendReponse
	 * Details	: Send the response back to the client after task execution. 
	 * 		  Response will be sent through the SQS reserved for respective client.
	 */
	private void sendReponse(Task.Builder task){

		String stringTask = new String(Base64.encode(task.build().toByteArray()));
		sqs.sendMessage(new SendMessageRequest(QueueUrlPrefix+task.getClientId(), stringTask));
	}

	/*
	 * Function	: getQueueLength
	 * Details	: Get the length of any SQS. 
	 * 		  Used here to check whether are there any messages present in the SQS or not.
	 */
	public int getQueueLength(String queueUrl){
		HashMap<String, String> attributes;
		sqs = new AmazonSQSClient(new ClasspathPropertiesFileCredentialsProvider());
		Region usEast1 = Region.getRegion(Regions.US_EAST_1);
		sqs.setRegion(usEast1);
		Collection<String> attributeNames = new ArrayList<String>();
		attributeNames.add("ApproximateNumberOfMessages");
		GetQueueAttributesRequest getAttributesRequest = new GetQueueAttributesRequest(queueUrl)
		.withAttributeNames(attributeNames);
		attributes = (HashMap<String, String>) sqs.getQueueAttributes(getAttributesRequest).getAttributes();
		//int totalMessages = Integer.valueOf(attributes.get("ApproximateNumberOfMessages")) + Integer.valueOf(attributes.get("ApproximateNumberOfMessagesNotVisible"));
		//return totalMessages;
		return Integer.valueOf(attributes.get("ApproximateNumberOfMessages"));
	}

	/*
	 * Function : getWorkerIpAddress
	 * Details  : Get the IP address of the worker.
	 */
	public String getWorkerIpAddress() {

		String ipAddress = null;
		Enumeration<NetworkInterface> net= null;
		try{
			net = NetworkInterface.getNetworkInterfaces();
			NetworkInterface netinterface = net.nextElement();
			Enumeration<InetAddress> addresses = netinterface.getInetAddresses();
			while (addresses.hasMoreElements()){
				InetAddress ip = addresses.nextElement();
				if (ip instanceof Inet4Address){
					ipAddress = ip.getHostAddress();
				}
			}
		}
		catch(SocketException se){
			se.printStackTrace();
		}
		return ipAddress;
	}

	/* 
	 * Function	: pullAndDelete
	 * Details	: Check are there any task present in the global request queue and 
	 *            HPC worker manage queue and get the task for the worker and work on it.
	 */
	public void pullAndDelete(boolean duplicateCheck) {
		// Receive 1 messages at most

		byte[] byteTask;
		byte[] byteHpcMessage;
		byte[] encoded;
		String msg;
		HashMap<String, String> attributes;
		boolean isEmpty=false;
		boolean wasEmpty = false;
		boolean secEmpty = false;
		String messageRecieptHandle;
		Task.Builder task = Task.newBuilder();
		hpcWorkerMessage.Builder hpcQueueMsg = hpcWorkerMessage.newBuilder();        // Pank-Hpc
		String ownWorkerIp = getWorkerIpAddress();
		int run = 0;
		//int hpcMsgCnt = 1;
		//int globalMsgCnt = 1;

		Random rand = new Random();
		int randomNumber = rand.nextInt(100);
		threadSleep(randomNumber);
		while (!isEmpty && run < 5) { //keeps fetching it's empty.
			//threadSleep(10000);
			try{

				// If HPC Worker Manager Queue is not empty then take up a
				// message from it, to get a task
				//int hpcQueueSize = getQueueLength(requestHpcWorkerMessageQueueUrl);
				//int globalQueueSize = getQueueLength(requestQueueUrl);

				if (isEmpty==false && (getQueueLength(requestHpcWorkerMessageQueueUrl) > 0)) {
					// HPC Worker queue is not empty, that means at least one
					// worker is waiting for a free worker to share HPC task.

					//System.out.println("\n\n ****** RUNNING AS A CLIENT ****** \n");
					//System.out.println(hpcMsgCnt + ") Hey there!! I am running now as a client.");
					//System.out.println("Just now, I checked HPC Queue, its size was: " + hpcQueueSize);
					//hpcMsgCnt++;

					ReceiveMessageRequest receiveHpcMessageRequest = new ReceiveMessageRequest(requestHpcWorkerMessageQueueUrl).withMaxNumberOfMessages(1);
					List<Message> hpcMessages = sqs.receiveMessage(receiveHpcMessageRequest).getMessages();
					//System.out.println("Info\t: HPC Message Size: " + hpcMessages.size());
					//System.out.println("Info\t: Is it empty? " + hpcMessages.isEmpty());

					while ((hpcMessages.size() == 0) && (getQueueLength(requestHpcWorkerMessageQueueUrl) > 0)) {
						// Keep on checking the hpc queue, if we get its size greater than 0
						// and if still worker does not pick any message from it.
						hpcMessages = sqs.receiveMessage(receiveHpcMessageRequest).getMessages();

						//System.out.println("Info\t: HPC Message Size: " + hpcMessages.size());
						//System.out.println("Info\t: Is it empty? " + hpcMessages.isEmpty());
					}

					if (!hpcMessages.isEmpty()) {
						messageRecieptHandle = hpcMessages.get(0).getReceiptHandle();
						msg = hpcMessages.get(0).getBody();
						attributes = (HashMap<String, String>) hpcMessages.get(0).getAttributes();

						// Retrieve actual message i.e. decode it.
						byteHpcMessage = Base64.decode(msg.getBytes());
						hpcQueueMsg.mergeFrom(byteHpcMessage);
						long hpcTaskId = hpcQueueMsg.getTaskId();
						String workerManagerIp = hpcQueueMsg.getWorkerIp();
						int workerManagerPort = hpcQueueMsg.getWorkerSocketPort();
						int pickUpCount = hpcQueueMsg.getPickUpCount();

						//System.out.println("Info\t: HPC Task Id is: " + hpcTaskId);
						//System.out.println("Info\t: Worker Manager's IP is: " + workerManagerIp);
						//System.out.println("Info\t: Worker manger port: " + workerManagerPort);
						//System.out.println("Info\t: My IP is: " + ownWorkerIp);
						//System.out.println("Info\t: My port number is: " + workerThreadPort);
						//System.out.println("");
						System.out.println("Info\t: Running as Sub worker[" +ownWorkerIp+ ":" +workerThreadPort+ "] with manager[" +workerManagerIp+ ":" +workerManagerPort+ "] on task "+hpcTaskId);
						// Delete the massage once important information is extracted.
						//System.out.println("Message\t: Deleting message: "+ messageRecieptHandle);
						try {
							sqs.deleteMessage(new DeleteMessageRequest( requestHpcWorkerMessageQueueUrl, messageRecieptHandle));

							if (pickUpCount < 5) {
								if (!(workerManagerIp.equalsIgnoreCase(ownWorkerIp)) || (workerManagerPort != workerThreadPort)){
									try {
										//System.out.println("Info\t: Creating interface to connect server");
										runWorker wm = new runWorker();
										int retCode = wm.runClient(hpcTaskId, workerManagerIp, workerManagerPort, ownWorkerIp, workerThreadPort, createRegistry);
										//System.out.println("Info\t: Return code is: " + retCode);
										if(retCode != 0) {
											// Something went wrong while executing as client. Putting message back into the HPC worker queue.
											System.out.println("ERROR\t: Faced some issues while working with this task.");
											//System.out.println("Message\t: Putting message back into the queue.");

											hpcQueueMsg.setTaskId(hpcTaskId);
											hpcQueueMsg.setWorkerIp(workerManagerIp);
											hpcQueueMsg.setWorkerSocketPort(workerManagerPort);
											hpcQueueMsg.setPickUpCount((pickUpCount+1));
											//hpcQueueMsg.setTaskPriority(task.getTaskPriority);
											encoded = hpcQueueMsg.build().toByteArray();
											String stringTask = new String(Base64.encode(encoded));

											sqs.sendMessage(new SendMessageRequest(requestHpcWorkerMessageQueueUrl, stringTask));
										}
										createRegistry = false;
									} catch (MalformedURLException me) {
										me.printStackTrace();
									} catch (RemoteException re) {
										re.printStackTrace();
									} catch (NotBoundException nbe) {
										nbe.printStackTrace();
									} catch (InterruptedException ie) {
										ie.printStackTrace();
									}
								}
							} else {
								System.out.println("WARNING\t: This message seems to be a stale message.");
							}
						} catch (AmazonServiceException ase) {
							System.out.println("ERROR: Faced some issues while deleting the message.");
							System.out.println(ase.getMessage());
							ase.printStackTrace();
						} catch (AmazonClientException ace) {
							System.out.println("ERROR: Faced some issues while deleting the message.");
							System.out.println(ace.getMessage());
							ace.printStackTrace();
						}
					}
				} else if(isEmpty==false && (getQueueLength(requestQueueUrl) > 0)) {
					// As HPC Worker Manager Queue is empty, get the task from Global request queue.

					//System.out.println("\n\n ****** RUNNING AS A MANAGER ****** \n");
					//System.out.println(globalMsgCnt + ") Eeehooo!! I am Master of workers (mow). Oh yeah!");
					//globalMsgCnt++;
					ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(requestQueueUrl).withMaxNumberOfMessages(processMaxCount);
					long receiveTime = System.currentTimeMillis();
					receiveMessageRequest.setAttributeNames(attributeNames);
					List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
					//System.out.println("Info\t: Global Queue which had a size: " + globalQueueSize);
					//System.out.println("Info\t: Global Message Size: " + messages.size());

					if (!messages.isEmpty()) {
						// If worker receives any task in the message list then start working on them.

						for (int i = 0; i < messages.size(); i++) {
							// Work on each message one by one.

							//int taskNum = i+1;
							//System.out.println("Info\t: Working on task " + taskNum);
							messageRecieptHandle = messages.get(i).getReceiptHandle();
							msg = messages.get(i).getBody();
							attributes = (HashMap<String, String>) messages.get(i).getAttributes();
							byteTask = Base64.decode(msg.getBytes());                         
							task.mergeFrom(byteTask);

							//delete 1 msg
							//System.out.println("	Deleting message number: " + i);
							sqs.deleteMessage(new DeleteMessageRequest( requestQueueUrl, messageRecieptHandle));                
							//retrieve Task


							if (duplicateCheck) {
								// Check whether the received from global request queue task is duplicate or not i.e.
								// has this task been picked up by any other worker. DynamoDB is used for the same.

								try {
									//System.out.println("Info\t: We will check for task duplicacy");
									addItem(tableName, task.getClientId(), task.getTaskId(), workerId);// put it in ddb

									if(task.getNumWorkers() > 1) {                             /* Pank-Hpc */
										// Its a HPC task. So this is a worker manager.

										//System.out.println("\n\n ---------- HPC TASK " + task.getTaskId() + " -------- \n");
										//System.out.println("Info\t:	Ohh yeah!! Its HPC task. I like it!!");
										//System.out.println("Info\t:	Its time to manage other workers and get the work done.. :D");
										//System.out.println("Info\t: Required Number of Workers: " + task.getNumWorkers());
										// Set the task id , worker IP and worker port for the HPC Q message

										/* get number of workers and managers;
										 * check if they are less than the threshold
										 * if ok go ahead
										 * else check hpc
										 *  if (threshold value is satisfied)
										 *  {
										 *  put values to do ddb  
										 */
										boolean grantedHpcTask = false;
										try {
											//System.out.println("Checking for DDB requirements");
											grantedHpcTask = hpcDynamo.ForWorkerCreationCheck(task.getNumWorkers(),this.totNumOfWorkers);
										} catch (AmazonServiceException ase) {
											// Caught an exception while working with dynamoDB.
											// No need to run the task and put back the message into the queue.
											grantedHpcTask = false;
											System.out.println("ERROR\t: Faced issues while running DynamoDB");
											System.out.println(ase.getMessage());
											ase.printStackTrace();
										} catch (AmazonClientException ace) {
											// Caught an exception while working with dynamoDB.
											// No need to run the task and put back the message into the queue.
											grantedHpcTask = false;
											System.out.println("ERROR\t: Faced issues while running DynamoDB");
											System.out.println(ace.getMessage());
											ace.printStackTrace();
										}

										if (grantedHpcTask){
											int retCode = 1;
											/*hpcQueueMsg.setTaskId(task.getTaskId());
											hpcQueueMsg.setWorkerIp(ownWorkerIp);
											hpcQueueMsg.setWorkerSocketPort(workerThreadPort);
											hpcQueueMsg.setPickUpCount(0);
											//hpcQueueMsg.setTaskPriority(task.getTaskPriority);
											encoded = hpcQueueMsg.build().toByteArray();
											String stringTask = new String(Base64.encode(encoded));*/

											System.out.println("Info\t: Running as a manager[" + ownWorkerIp + ":" + workerThreadPort + "] on task "+task.getTaskId());
											try {
												/*for(int num = 1; num < task.getNumWorkers(); num++) {
													//System.out.println("Info\t: Message " + num + ": Putting message into the HPC woker manager queue");
													sqs.sendMessage(new SendMessageRequest(requestHpcWorkerMessageQueueUrl, stringTask));
												}*/

												try {
													//System.out.println("Info\t: Creating interface to accept connections");
													runWorker wm = new runWorker();
													retCode = wm.runServer(task.getTaskId(), Integer.valueOf(task.getBody()), ownWorkerIp, workerThreadPort, 1, task.getNumWorkers(), createRegistry, this.totNumOfWorkers);
													//System.out.println("Info\t: Return code is: " + retCode);
													createRegistry = false;

												} catch (MalformedURLException me) {
													me.printStackTrace();
												} catch (RemoteException re) {
													re.printStackTrace();
												} catch (NotBoundException nbe) {
													nbe.printStackTrace();
												} catch (ExecutionException ee) {
													ee.printStackTrace();
												} catch (InterruptedException ie) {
													ie.printStackTrace();
												}
											} catch (AmazonServiceException se) {
												System.out.println("ERROR\t: Faced issues while putting message into HPC woker manager queue. ");
												System.out.println(se.getMessage());
												se.printStackTrace();

											} catch (AmazonClientException ce) {
												System.out.println("ERROR\t: Faced issues while putting message into HPC woker manager queue. ");
												System.out.println(ce.getMessage());
												ce.printStackTrace();
											}
											finally{
												hpcDynamo.deleteFromExisting(1, task.getNumWorkers());
											}
											/* finally delete all the workers that were used
											 * for this task 										 * 
											 */
											if (retCode == 0) {

												System.out.println("Success\t: Sending response back to client.");
												task.setReceiveTime(receiveTime);		//set the time
												//task.setSendTime(Long.valueOf(attributes.get("SentTimestamp")));
												task.setCompleteTime(System.currentTimeMillis());
												sendReponse(task);		//Done! send the response
												// Response to the client is sent back successfully. Now delete the message from queue.
												//System.out.println("Message\t: Deleting task message from the queue: " + taskNum);

											} else {
												System.out.println("ERROR\t: Task was not completed successfully.");
												//System.out.println("ERROR\t: No responce will be sent to client.");
												sqs.sendMessage(new SendMessageRequest(requestQueueUrl, msg));
											}
										} else {
											//System.out.println("WARNING\t: Got rejection to work on this HPC job.");
											sqs.sendMessage(new SendMessageRequest(requestQueueUrl, msg));
										}
									} else {
										// If MTC task

										//System.out.println("\n\n ---------- MTC TASK " + task.getTaskId() + " -------- \n");
										//System.out.println("Info\t: Yuppy!! Its MTC task.");
										//System.out.println("Info\t: I can do it on my own. I do not need any help :P :P");
										//System.out.println("Info\t: Number of workers are: "+ task.getNumWorkers());
										//It is a MTC task directly run the task i.e. call function runTask().
										isBusy = true;
										threadSleep(Long.valueOf(task.getBody()));//assuming task body is sleep length
										isBusy = false;
										task.setReceiveTime(receiveTime);		//set the time
										//task.setSendTime(Long.valueOf(attributes.get("SentTimestamp")));
										task.setCompleteTime(System.currentTimeMillis());
										sendReponse(task);		//Done! send the response
									}

								} catch (ConditionalCheckFailedException e) {
									duplicate++;
									// Put the task back.
								}    
							} else {
								//If duplicate check is not being performed

								//System.out.println("Info\t: Check for duplicacy is disabled");
								if(task.getNumWorkers() > 1) {
									// Its a HPC task. So this is a worker manager.

									//System.out.println("\n\n ---------- HPC TASK " + task.getTaskId() + " -------- \n");
									//System.out.println("Info\t:	Ohh yeah!! Its HPC task. I like it!!");
									//System.out.println("Info\t:	Its time to manage other workers and get the work done.. :D");
									//System.out.println("Info\t: Required Number of Workers: " + task.getNumWorkers());
									// Set the task id , worker IP and worker port for the HPC Q message

									/* get number of workers and managers;
									 * check if they are less than the threshold
									 * if ok go ahead
									 * else check hpc
									 *  if (threshold value is satisfied)
									 *  {
									 *  put values to do ddb  
									 */
									boolean grantedHpcTask = false;
									try {
										grantedHpcTask = hpcDynamo.ForWorkerCreationCheck(task.getNumWorkers(),this.totNumOfWorkers);
									} catch (AmazonServiceException ase) {
										// Caught an exception while working with dynamoDB.
										// No need to run the task and put back the message into the queue.
										grantedHpcTask = false;
										System.out.println("ERROR\t: Faced issues while running DynamoDB");
										System.out.println(ase.getMessage());
										ase.printStackTrace();
									} catch (AmazonClientException ace) {
										// Caught an exception while working with dynamoDB.
										// No need to run the task and put back the message into the queue.
										grantedHpcTask = false;
										System.out.println("ERROR\t: Faced issues while running DynamoDB");
										System.out.println(ace.getMessage());
										ace.printStackTrace();
									}

									if (grantedHpcTask){
										int retCode = 1;
										/*long taskStartTime = System.currentTimeMillis();

										if(task.getNumWorkers() > prevTaskNumWorkers) {
											timeoutThreshold = (timeoutThreshold + (prevTaskTimeTaken * task.getNumWorkers() / prevTaskNumWorkers))/2;
										} else {
											timeoutThreshold = (timeoutThreshold + prevTaskTimeTaken) / 2;
										}
										prevTaskNumWorkers = task.getNumWorkers();*/
										/*hpcQueueMsg.setTaskId(task.getTaskId());
										hpcQueueMsg.setWorkerIp(ownWorkerIp);
										hpcQueueMsg.setWorkerSocketPort(workerThreadPort);
										hpcQueueMsg.setPickUpCount(0);
										//hpcQueueMsg.setTaskPriority(task.getTaskPriority);
										encoded = hpcQueueMsg.build().toByteArray();
										String stringTask = new String(Base64.encode(encoded));*/

										System.out.println("Info\t: Running as a manager[" + ownWorkerIp + ":" + workerThreadPort + "] on task "+task.getTaskId());
										try {
											/*for(int num = 1; num < task.getNumWorkers(); num++) {
												//System.out.println("Info\t: Message " + num + ": Putting message into the HPC woker manager queue");
												sqs.sendMessage(new SendMessageRequest(requestHpcWorkerMessageQueueUrl, stringTask));
											}*/

											try {
												//System.out.println("Info\t: Creating interface to accept connections");
												runWorker wm = new runWorker();
												retCode = wm.runServer(task.getTaskId(), Integer.valueOf(task.getBody()), ownWorkerIp, workerThreadPort, 1, task.getNumWorkers(), createRegistry, this.totNumOfWorkers);
												//System.out.println("Info\t: Return code is: " + retCode);
												createRegistry = false;

											} catch (MalformedURLException me) {
												me.printStackTrace();
											} catch (RemoteException re) {
												re.printStackTrace();
											} catch (NotBoundException nbe) {
												nbe.printStackTrace();
											} catch (ExecutionException ee) {
												ee.printStackTrace();
											} catch (InterruptedException ie) {
												ie.printStackTrace();
											}
										} catch (AmazonServiceException se) {
											System.out.println("ERROR\t: Faced issues while putting message into HPC woker manager queue. ");
											System.out.println(se.getMessage());
											se.printStackTrace();

										} catch (AmazonClientException ce) {
											System.out.println("ERROR\t: Faced issues while putting message into HPC woker manager queue. ");
											System.out.println(ce.getMessage());
											ce.printStackTrace();
										}
										finally{
											hpcDynamo.deleteFromExisting(1, task.getNumWorkers());
										}
										/* finally delete all the workers that were used
										 * for this task 										 * 
										 */
										if (retCode == 0) {

											System.out.println("Success\t: Sending response back to client.");
											task.setReceiveTime(receiveTime);		//set the time
											//task.setSendTime(Long.valueOf(attributes.get("SentTimestamp")));
											task.setCompleteTime(System.currentTimeMillis());
											sendReponse(task);		//Done! send the response
											// Response to the client is sent back successfully. Now delete the message from queue.
											//System.out.println("Message\t: Deleting task message from the queue: " + taskNum);

										} else {
											System.out.println("ERROR\t: Task was not completed successfully.");
											//System.out.println("ERROR\t: No responce will be sent to client.");
											sqs.sendMessage(new SendMessageRequest(requestQueueUrl, msg));
										}
										/*long taskEndTime = System.currentTimeMillis();
										prevTaskTimeTaken = taskEndTime - taskStartTime;*/
									} else {
										//System.out.println("WARNING\t: Got rejection to work on this HPC job.");
										sqs.sendMessage(new SendMessageRequest(requestQueueUrl, msg));
									}


								} else {
									// If task is MTC

									//System.out.println("\n\n ---------- MTC TASK " + task.getTaskId() + " -------- \n");
									//System.out.println("Info\t: Yuppy!! Its MTC task.");
									//System.out.println("Info\t: I can do it on my own. I do not need any help :P :P");
									//System.out.println("Info\t: Number of workers are: "+ task.getNumWorkers());
									//It is a MTC task directly run the task i.e. call function runTask().
									isBusy = true;
									threadSleep(Long.valueOf(task.getBody()));//assuming task body is sleep length
									isBusy = false;
									task.setReceiveTime(receiveTime);		//set the time
									//task.setSendTime(Long.valueOf(attributes.get("SentTimestamp")));
									task.setCompleteTime(System.currentTimeMillis());
									sendReponse(task);		//Done! send the response
								}
							}
						}    
					}
					//} else if ((getQueueLength(requestQueueUrl) > 0) || (getQueueLength(requestHpcWorkerMessageQueueUrl) > 0)) {

				} else {
					run++;
					if(run == 5) {
						isEmpty = true;
						isDone = true;
					}
				}

			} catch (AmazonServiceException ase) {
				System.out.println("Caught an AmazonServiceException, which means your request made it " +
						"to Amazon SQS, but was rejected with an error response for some reason.");
				System.out.println("Error Message:    " + ase.getMessage());
				System.out.println("HTTP Status Code: " + ase.getStatusCode());
				System.out.println("AWS Error Code:   " + ase.getErrorCode());
				System.out.println("Error Type:       " + ase.getErrorType());
				System.out.println("Request ID:       " + ase.getRequestId());
			} catch (AmazonClientException ace) {
				System.out.println("Caught an AmazonClientException, which means the client encountered " +
						"a serious internal problem while trying to communicate with SQS, such as not " +
						"being able to access the network.");
				System.out.println("Error Message: " + ace.getMessage());
			} catch (InvalidProtocolBufferException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}
	@Override

	/*    public void runTask(task) {
        isBusy = true;
        threadSleep(Long.valueOf(task.getBody()));//assuming task body is sleep length
        isBusy = false;
        //set the time
        task.setReceiveTime(receiveTime);
        //task.setSendTime(Long.valueOf(attributes.get("SentTimestamp")));
        task.setCompleteTime(System.currentTimeMillis());
        //Done! send the response
        sendReponse(task);
    } */

	public void run() {

		if (monitoring) {
			Thread monitorThread = new Thread(new MonitorThread());
			monitorThread.start();
			pullAndDelete(duplicateCheck);
			if (duplicateCheck) {
				System.out.println("duplicates #:"+ duplicate);
			}
			try {
				monitorThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			pullAndDelete(duplicateCheck);
			if (duplicateCheck) {
				System.out.println("duplicates #:"+ duplicate);
			}
		}


	}

	//    private static String getItemWorkerId(String tableName, String clientId, long taskId){//not used anymore
	//        Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
	//        item.put("messageId", new AttributeValue(clientId+String.valueOf(taskId)));
	//        GetItemRequest getItemRequest = new GetItemRequest(tableName, item);
	//        GetItemResult getItemResult = dynamoDB.getItem(getItemRequest);
	//        if (!getItemResult.toString().equals("{}")){
	//            item= getItemResult.getItem();
	//            return item.get("workerId").getS();
	//        } else
	//            return "null";
	//    }

	private static void updateMonitor(String tableName,boolean isBusy) {//only busy for now
		Map<String, AttributeValueUpdate> updateItems = new HashMap<String, AttributeValueUpdate>();
		HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
		key.put("id", new AttributeValue().withN("1"));
		if (isBusy) {
			updateItems.put("busy", new AttributeValueUpdate().withAction(AttributeAction.ADD)
					.withValue(new AttributeValue().withN("+1")));    
		} else {
			updateItems.put("free", new AttributeValueUpdate().withAction(AttributeAction.ADD)
					.withValue(new AttributeValue().withN("+1")));    
		}

		UpdateItemRequest updateItemRequest = new UpdateItemRequest().withTableName(tableName)
				.withAttributeUpdates(updateItems);

		dynamoDB.updateItem(updateItemRequest);
	}
	private static void addItem(String tableName, String clientId, long taskId, String workerId) {
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put("messageId", new AttributeValue(clientId+String.valueOf(taskId)));
		item.put("workerId", new AttributeValue(workerId));
		ExpectedAttributeValue notExpected = new ExpectedAttributeValue(false);
		Map<String, ExpectedAttributeValue> expected = new HashMap<String, ExpectedAttributeValue>();
		expected.put("messageId", notExpected);
		PutItemRequest putItemRequest = new PutItemRequest().withTableName(tableName)
				.withItem(item).withExpected(expected);
		dynamoDB.putItem(putItemRequest);
	}

	public class MonitorThread implements Runnable{

		@Override
		public void run() {

			try {
				while (!isDone){
					updateMonitor("monitor", isBusy);
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}